// window 1 // 

let pulsanteStart = document.querySelector(".inizia");
let window1 = document.querySelector(".window1");
let divForNext = document.querySelector(".divForNext");
// window 2 //

let window2 = document.querySelector(".window2");
let answersContainer = document.querySelector(".answers-container");

// Questions //

let domande = document.querySelector(".domande");
let laDomanda = document.querySelector("#laDomanda");

// With this function I can start the quiz //

let startQuiz = () => {
  window1.style.display = "none";
  window2.style.display = "flex";
};

pulsanteStart.addEventListener("click", () => {
  startQuiz();
  generate10QuestionIds()
  generateQuestion();
  generateAnswers()
  displayAnswers();
  displayNext();
});

// ARRAY DOMANDE //

const nations = [
  "Italy",
  "France",
  "Spain",
  "England",
  "Germany",
  "Portugal",
  "USA",
  "Colombia",
  "Greece",
  "Brazil",
  "Marocco",
  "Argentina",
  "Tunisia",
  "Jamaica",
  "Mexico",
  "Congo",
  "South Africa",
  "Poland",
  "Russia",
  "Paraguay",
  "Norway",
  "Austria",
  "Australia",
  "Vietnam",
  "Chile",
  "Turkey",
  "Zimbabwe",
  "Egypt",
  "Cyprus",
  "Switzerland"
];

// ARRAY ANSWERS (FIRST ONE THE RIGHT ONE) //

let answers = [
  ["Rome", "Milan", "Florence", "Naples"],
  ["Paris", "Marseille", "Toulouse", "Grenoble"],
  ["Madrid", "Barcelona", "Cordoba", "Valencia"],
  ["London", "Liverpool", "Manchester", "Bristol"],
  ["Berlin", "Hamburg", "Munich", "Cologne"],
  ["Lisbon", "Santa Cruz", "Porto", "Faro"],
  ["Washington", "New York", "Los Angeles", "Miami"],
  ["Bogotà", "Cali", "Buenaventura", "Santa Marta"],
  ["Athens", "Corinto", "Larissa", "Patras"],
  ["Brasilia", "Rio de Janeiro", "San Paolo", "Salvador"],
  ["Rabat", "Casablanca", "Marrakech", "Tangeri"],
  ["Buenos Aires", "Mendoza", "Mar de Plata", "Rosario"],
  ["Tunisi", "Cartagine", "Jerba", "Ariana"],
  ["Kingston", "Montego Bay", "Spanish Town", "Mandeville"],
  ["City of Mexico", "Tijuana", "Puebla", "Guadalajara"],
  ["Brazzaville", "Goma", "Bakavu", "Kinshasa"],
  ["Cape Town", "Pretoria", "Bloemfontein", "Port Elizabeth"],
  ["Varsavia", "Krakóv", "Lubin", "Radom"],
  ["Moscow", "Saint Petersburg", "Novosibirsk", "Kazan"],
  ["Asunción", "Ciudad del Este", "Encarnación", "Limpio"],
  ["Oslo", "Bergen", "Stravanger", "Lillehammer"],
  ["Vienna", "Salzburg", "Linz", "Graz"],
  ["Camberra", "Sidney", "Melbourne", "Darwin"],
  ["Hanoi", "Ho Chi Minh", "Nha Trang", "Haiphong"],
  ["Santiago del Chile", "La Serena", "Valdivia", "Valparaíso"],
  ["Ankara", "Istanbul", "Adalia", "Smirne"],
  ["Harare", "Mutare", "Bulawayo", "Kadoma"],
  ["Il Cairo", "Alexandria", "Luxor", "Giza"],
  ["Nicosia", "Famagusta", "Paphos", "Limassol"],
  ["Berne", "Geneva", "Lugano", "Zürich"]
];

//  EVALUATION QUIZ

let correctAnswers = []
let answerSelected = false
let tenQuestions = []
let tenQuestionsNoDup = []

let counter = -1
let questionId
// FUNZIONE CHE GENERA LE DOMANDE //

let generateQuestion = () => {
  counter++
  if (counter <=9){
    console.log('COUNTER', counter)
    questionId = tenQuestionsNoDup[counter]
    let randomNations = nations[tenQuestionsNoDup[counter]]
    let questions = `What's ${randomNations}'s capital?`;
    laDomanda.innerHTML = questions;
  }
 
};

// FUNZIONE CHE GENERA LE RISPOSTE ALLE RISPETTIVE DOMANDE //

let mixedAnswers = [];
let mixedAnswersNoDup = [];
let cities4

let generateAnswers = () => {

cities4 = answers[questionId]
let capital = cities4[0]
while (mixedAnswersNoDup.length < 4) {
  let randomNumber = Math.floor(Math.random() * cities4.length)
  let city = cities4[randomNumber]
  mixedAnswers.push(city)
  mixedAnswersNoDup = mixedAnswers.filter((el, index) => {
    return mixedAnswers.indexOf(el) === index;
  })
}  

return capital
}

let displayAnswers = () => {
 
  mixedAnswersNoDup.forEach(el => {
    let divAns = document.createElement("div");
    divAns.className = "answer";
    let p = document.createElement("p");
    p.innerHTML = el;
    divAns.appendChild(p);
    answersContainer.appendChild(divAns);

    divAns.addEventListener('click', ()=> {

      let answers1 = document.querySelectorAll('.answer')
      for(let i = 0; i < answers1.length; i ++){
        answers1[i].classList.remove('answerSelected')
        answers1[i].className = 'answer'
      }
       divAns.classList.toggle('answerSelected')
       pushCorrectQuestionIntoArray(el)
    })
  
  });
};


let displayNext = () => {
 
  let next = document.createElement("button");
  next.className = "next";
  next.innerHTML = "Next";
  divForNext.appendChild(next);

  next.addEventListener("click", () => {
    if(answerSelected) {
      pushCorrectQuestionIntoArray1()
      if (counter <= 9) {
        mixedAnswers = [];
        mixedAnswersNoDup = [];
        laDomanda.innerHTML = "";
        answersContainer.innerHTML = "";
        generateQuestion();
        generateAnswers();
        displayAnswers();
      }
      
      if(counter === 9){
        next.innerHTML = "Show result";
      }
        if(counter > 9) {
          next.style.display = 'none'
          showResult()
          let startAgain = document.createElement("button");
            divForNext.appendChild(startAgain);
            startAgain.className = "next";
            startAgain.innerHTML = "Try again";
            
            startAgain.addEventListener('click', ()=> {
                startAgain.style.display = 'none'
                counter = -1
                correctAnswers = []
                mixedAnswers = [];
                mixedAnswersNoDup = []; 
                tenQuestions = []
                tenQuestionsNoDup = []
                answerSelected = false
                window2.style.display = 'none'
                window1.style.display = 'flex'
            })
          }
        }  
      
        answerSelected = false
    });
        }

        if (counter > 9) {
        laDomanda.innerHTML = "";
        answersContainer.innerHTML = "";
        };


let generate10QuestionIds = ()=> {

  while (tenQuestionsNoDup.length < 10) {
    let randomNumber = Math.floor(Math.random() * nations.length);
    tenQuestions.push(randomNumber);
    tenQuestionsNoDup = tenQuestions.filter((el, index) => {
      return tenQuestions.indexOf(el) === index;
    });

}

console.log('TEN QUESTION NO DUP', tenQuestionsNoDup)

}


let showResult = ()=> {
  answersContainer.innerHTML = "";
  if(correctAnswers.length <= 4) {
    laDomanda.innerHTML = `Your score is ${correctAnswers.length}/10. You might need to revise you geography knowledge.`;
  }
  else if(correctAnswers.length >= 5 && correctAnswers.length < 8) {
    laDomanda.innerHTML = `Good job! Your score is ${correctAnswers.length}/10!`;
  }
  else if(correctAnswers.length >= 8) {
    laDomanda.innerHTML = `Fantastic! Your score is ${correctAnswers.length}/10!`;
  }
}


let pushCorrectQuestionIntoArray = ()=> {
 
   let answer2 = document.querySelectorAll('.answer')

   for(let i = 0; i < answer2.length; i++){
    let answerValue = answer2[i].children[0].innerHTML
    if(answer2[i].classList.contains('answerSelected')) {
      answerSelected = answerValue
    } 
  }
  return answerSelected
}

let pushCorrectQuestionIntoArray1 = ()=> {

    if (answerSelected === cities4[0]) {
      correctAnswers.push(answerSelected)
    } 
   console.log(correctAnswers)
}